﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InOutManager : MonoBehaviour
{
    public Dictionary<Vector3,BuildingInput> buildingInputs;
    public Dictionary<Vector3, BuildingOutput> buildingOutputs;

    private void Start()
    {
        buildingInputs = new Dictionary<Vector3, BuildingInput>();
        buildingOutputs = new Dictionary<Vector3, BuildingOutput>();
    }

    public void AddInput(BuildingInput _input,Vector3 _worldPos)
    {
        buildingInputs.Add(UtilFunctions.Instance.SnapToGrid(_worldPos),_input);
    }

    public void AddOutput(BuildingOutput _output, Vector3 _worldPos)
    {
        buildingOutputs.Add(UtilFunctions.Instance.SnapToGrid(_worldPos), _output);
    }

    public BuildingInput GetInputAt(Vector3 _worldPos)
    {
        BuildingInput input;
        buildingInputs.TryGetValue(UtilFunctions.Instance.SnapToGrid(_worldPos),out input);
        return input;
    }
    public BuildingOutput GetOutputAt(Vector3 _worldPos)
    {
        BuildingOutput output;
        buildingOutputs.TryGetValue(UtilFunctions.Instance.SnapToGrid(_worldPos), out output);
        return output;
    }

    public void RemoveInputAt(Vector3 _worldPos)
    {

        if(buildingInputs.ContainsKey(UtilFunctions.Instance.SnapToGrid(_worldPos)))
        {
            buildingInputs.Remove(UtilFunctions.Instance.SnapToGrid(_worldPos));
        }
    }
    public void RemoveOutputAt(Vector3 _worldPos)
    {

        if (buildingOutputs.ContainsKey(UtilFunctions.Instance.SnapToGrid(_worldPos)))
        {
            buildingOutputs.Remove(UtilFunctions.Instance.SnapToGrid(_worldPos));
        }
    }

}
