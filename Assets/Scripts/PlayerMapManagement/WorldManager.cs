﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldManager : MonoBehaviour
{
    public GameObject metalResourcePrefab;
    public Dictionary<Vector3,GameElement> worldResources = new Dictionary<Vector3, GameElement>();

    //TODO FUNCTION TO READ ALREADY CREATED RESOURCES
   
    public void AddNewResourceFromMap(Vector3 _worldPos,ResourceType _type)
    {
        if(!worldResources.ContainsKey(_worldPos))
        {
            Debug.Log("Adding resource AT " + _worldPos.ToString());
            GameObject newResource = Instantiate(metalResourcePrefab);
            GameElement newResourceElement = newResource.GetComponent<GameElement>();
            Vector3 gridPos = UtilFunctions.Instance.SnapToGrid(_worldPos);
            newResourceElement.data.worldPos = _worldPos;
            newResource.transform.position = _worldPos;
            worldResources.Add(_worldPos, newResourceElement);
        }
        else
        {
            Debug.Log("WARNING THERE IS ALREADY A RESOURCE AT " + _worldPos.ToString());
        }
    }

    public GameElement GetResourcesAtWorldPosition(Vector3 _worldPos)
    {
        GameElement gameElement;
        Vector3 gridPosition = UtilFunctions.Instance.SnapToGrid(_worldPos);
        worldResources.TryGetValue(gridPosition, out gameElement);
        return gameElement;
    }
}
