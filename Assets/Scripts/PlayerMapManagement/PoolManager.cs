﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolManager : MonoBehaviour
{
    public Pool collectibles;

    public GameObject GetCollectible(ItemType type)
    {
        switch(type)
        {
            case ItemType.Metal:
                GameObject newCollectible = collectibles.GetObject();
                newCollectible.GetComponent<Item>().itemType = type;
                return newCollectible;
        }
        return null;
    }
}
