﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMapManager : MonoBehaviour
{
    public static PlayerMapManager Instance;


    public UIPlayerMap uiPlayerMap;
    public WorldBuilder worldBuilder;
    public WorldManager worldManager;
    public CollectibleManager collectiblesManager;
    public PoolManager poolManager;
    public InOutManager inOutManager;
    public ElementSpawner elementSpawner;
    public SelectionManager selectionManager;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
        }
      
    }

    private void Start()
    {

    }
}
