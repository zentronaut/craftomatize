﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldBuilder : MonoBehaviour

{
    // TODO Load only used prefabs
    public GameObject wallPrefab;
    public GameObject metalPrefab;
    public GameObject robotPrefab;

    private int xMapOffset;
    private int zMapOffset;
    private int initMapWidth;
    private int initMapHeight;

    [SerializeField]
    private Transform wallsParent;
    [SerializeField]
    private Transform resourcesParent;
    [SerializeField]
    private Transform unitsParent;

    //TODO change this
    WorldMap worldMap;

    WorldManager worldManager;

    // Start is called before the first frame update
    void Start()
    {

        worldMap = GameManager.Instance.worldLoader.worldMap;
        worldManager = PlayerMapManager.Instance.worldManager;
        initMapWidth = worldMap.testWorld.GetLength(0);
        initMapHeight = worldMap.testWorld.GetLength(1);
        CalculateOffsetsToCenterMap();
       
        //TODO change loading sequence

        BuildWorld();
        PlayerMapManager.Instance.elementSpawner.SpawnElements();
    }

    void CalculateOffsetsToCenterMap()
    {
        xMapOffset = Mathf.FloorToInt(initMapWidth / 2);
        zMapOffset = Mathf.FloorToInt(initMapHeight / 2);
    }

    void BuildWorld()
    {

        for(int i=0; i< initMapWidth; i++)
        {
            for(int j=0; j < initMapHeight; j++)
            {
                CheckValue(worldMap.testWorld[j,i], i, j);
            }
        }
    }

    void CheckValue(int value,int x, int z)
    {

        Vector3 newPosition = new Vector3(x - xMapOffset, 0, zMapOffset - z );
        GameObject newElement;
        GameElement newGameElement;
       
        switch (value)
        {
            case 0:
                break;
            case 1:
                newGameElement = wallPrefab.GetComponent<GameElement>();
                newElement = Instantiate(wallPrefab, newPosition, Quaternion.Euler(0,(int)newGameElement.data.rotationDirection , 0));
                newElement.transform.SetParent(wallsParent);
                newElement.GetComponent<GameElement>().InitializeGameElement();
                break;
            case 2:
                worldManager.AddNewResourceFromMap(newPosition,ResourceType.Metal);
                break;

        }
      

    }

    public int GetValueAtWorldPosition(Vector3 worldPos)
    {
        int xPos = (int) worldPos.x + xMapOffset;
        int zPos = (int) -worldPos.z + zMapOffset;
        Debug.Log("Chacking value at " + xPos + " " + zPos);
        Debug.Log("Value result " + worldMap.testWorld[zPos, xPos]);
        return worldMap.testWorld[zPos, xPos];
    }
    public bool IsObstacleAtWorldPosition(Vector3 worldPos)
    {
        int xPos = (int)worldPos.x + xMapOffset;
        int zPos = (int)-worldPos.z + zMapOffset;
        Debug.Log("Chacking value at " + xPos + " " + zPos);
        Debug.Log("Value result " + worldMap.testWorld[zPos, xPos]);
        return worldMap.testWorld[zPos, xPos] != 0;
    }

    public void AddCollitionAt(Vector3 worldPos)
    {
        Vector3 gridPos = WorldToMatrix(worldPos);

        worldMap.testWorld[(int)gridPos.x, (int)gridPos.z] = 100;
    }
    public void AddCollitionsAt(Vector3[] worldPos)
    {
        for(int i=0; i< worldPos.Length; i++)
        {
            Vector3 gridPos = WorldToMatrix(worldPos[i]);

            worldMap.testWorld[(int)gridPos.x, (int)gridPos.z] = 100;
        }

       
    }
    public Vector3 WorldToMatrix(Vector3 _worldPos)
    {
        Vector3 matrixPos = new Vector3(_worldPos.x + xMapOffset, 0, zMapOffset - _worldPos.z);

        return UtilFunctions.Instance.SnapToGrid(matrixPos);
    }
}
