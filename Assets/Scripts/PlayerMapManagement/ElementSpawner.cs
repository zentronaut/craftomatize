﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ElementSpawner : MonoBehaviour
{

    WorldBuilder worldBuilder;
    public GameElementData[] elementsToSpawn;

    //TODO create prefab manager with all prefabs and load at runtime
    public GameObject sotrageBuildingPrefab;
    public GameObject smallRobotPrefab;

    private void Start()
    {
        worldBuilder = PlayerMapManager.Instance.worldBuilder;
    }

    public void SpawnElements()
    {
        for(int i=0; i< elementsToSpawn.Length;i++)
        {
            TryToSpawnElement(elementsToSpawn[i]);
        }
    }

  
   
    //TODO CHECK IF MUST GENERATE COLLITIONS ON WORLDMAP
    //TODO SPAWN RESOURCES HERE IN a COOL FUNCTION
    public void TryToSpawnElement(GameElementData _Data)
    {
        Quaternion rotation;
        switch (_Data.gameElementType)
        {
            case GameElementType.Building:
                GameElementDefinition buildingDefinition = GameManager.Instance.definitionsManager.GetBuildingDefinition(_Data.buildingType);
                int numberOfTilesNeededX = buildingDefinition.numberOfNeededTilesX;
                int numberOfTilesNeededZ = buildingDefinition.numberOfNeededTilesZ;
                Vector3[] tilesNeededInWorldPos = new Vector3[numberOfTilesNeededX * numberOfTilesNeededZ];
                Vector3 buildingWorldPos = _Data.worldPos;


                for (int i = 0; i < numberOfTilesNeededX; i++)
                {
                    for (int j = 0; j < numberOfTilesNeededZ; j++)
                    {
                        tilesNeededInWorldPos[i * j] = buildingWorldPos + (Vector3.one * i) + (Vector3.one * j);
                    }
                }

                if (CanSpawnAtPosition(tilesNeededInWorldPos,true))
                {

                    GameObject buildinPref = GetBuildingPrefab(_Data.buildingType);
                     rotation = Quaternion.Euler(0, (int)_Data.rotationDirection, 0);
                    GameObject newBuilding = Instantiate(buildinPref, _Data.worldPos, rotation);
                    newBuilding.GetComponent<GameElement>().data = _Data;
                    newBuilding.GetComponent<GameElement>().definition = buildingDefinition;
                    newBuilding.GetComponent<GameElement>().InitializeGameElement();
                }
                else
                {
                    Debug.Log("WARNING: Building " + _Data.buildingType.ToString() + " was unable to place at " + _Data.worldPos.ToString());
                }

                break;

            case GameElementType.Unit:

                if(CanSpawnAtPosition( new Vector3[] { _Data .worldPos},false))
                {

                
                GameElementDefinition unitDefinition = GameManager.Instance.definitionsManager.GetUnitDefinition(_Data.unitType);
                GameObject unitPrefab = GetUnitPrefab(_Data.unitType);
                 rotation = Quaternion.Euler(0, (int)_Data.rotationDirection, 0);
                GameObject newUnit = Instantiate(unitPrefab, _Data.worldPos, rotation);
                newUnit.GetComponent<GameElement>().data = _Data;
                newUnit.GetComponent<GameElement>().definition = unitDefinition;
                newUnit.GetComponent<GameElement>().InitializeGameElement();
                }
                else
                {
                    Debug.Log("WARNING: Unit " + _Data.unitType.ToString() + " was unable to place at " + _Data.worldPos.ToString());
                }

                break;
        }

       
    }


    /// <summary>
    /// Check if building can be placed on that position and adds collitios to worldbuilder
    /// </summary>
    bool CanSpawnAtPosition(Vector3[] tilesNeeded,bool addCollitionsToWorldmap)
    {
        if(!worldBuilder)
            worldBuilder = PlayerMapManager.Instance.worldBuilder;
        foreach (Vector3 tile in tilesNeeded)
        {
            if (worldBuilder.IsObstacleAtWorldPosition(tile))
                return false;
        }

        if(addCollitionsToWorldmap)
        PlayerMapManager.Instance.worldBuilder.AddCollitionsAt(tilesNeeded);

        return true;
    }

    GameObject GetBuildingPrefab(BuildingType _type)
    {
        switch(_type)
        {
            case BuildingType.StorageBuilding:
                return sotrageBuildingPrefab;

        }
        return null;
    }
    GameObject GetUnitPrefab(UnitType _type)
    {
        switch (_type)
        {
            case UnitType.SmallRobot:
                return smallRobotPrefab;

        }
        return null;
    }

}
