﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;

    public WorldLoader worldLoader;
    public DefinitionsManager definitionsManager;
    // Start is called before the first frame update
    void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
        }
      
    }

    private void Start()
    {
        AppManager.Instance.sceneManager.LoadSceneLayer(2);
    }

}
