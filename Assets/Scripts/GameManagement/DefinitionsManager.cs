﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefinitionsManager : MonoBehaviour
{
    /// <summary>
    /// This hold all item scriptable object definitions in the order of itemType Game enums
    /// </summary>
    public ItemDefinition[] itemDefinitions;
    public GameElementDefinition[] buildingDefinitions;
    public GameElementDefinition[] unitDefinitions;
    public GameElementDefinition[] resourceDefinitions;


    public ItemDefinition GetItemDefinition(ItemType type)
    {
        return itemDefinitions[(int)type];
    }
    public GameElementDefinition GetBuildingDefinition(BuildingType type)
    {
        return buildingDefinitions[(int)type];
    }
    public GameElementDefinition GetUnitDefinition(UnitType type)
    {
        return unitDefinitions[(int)type];
    }
    public GameElementDefinition GetResourceDefinition(ResourceType type)
    {
        return resourceDefinitions[(int)type];
    }
}
