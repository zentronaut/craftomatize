﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeManager : MonoBehaviour
{
    public float tickDelay;

    public delegate void TimeAction();
    public static TimeAction onGameTick;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("GameTick",0, tickDelay);
    }

     void GameTick()
    {
        if (onGameTick != null)
            onGameTick();
    }
}
