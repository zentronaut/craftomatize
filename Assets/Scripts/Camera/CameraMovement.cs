﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public Camera cam;
    [SerializeField]
    private float horizontalSpeed;
    [SerializeField]
    private float zoomSpeed;
   

    [SerializeField]
    private float _yPos;
    [SerializeField]
    private float maxZoom;
    [SerializeField]
    private float minZoom;

    [SerializeField]
    AnimationCurve rotationCurve;

    [SerializeField]
    float middleMouseButtonSpeedStrenght;

    [SerializeField]
    int screenMovingMargin;

    private const string VERTICAL_AXIS = "Vertical";
    private const string HORIZONTAL_AXIS = "Horizontal";
    private const string MOUSE_SCROLL_WHEEL = "Mouse ScrollWheel";

    private float xDelta;
    private float yDelta;
    private float zDelta;

    private int screenWidth;
    private int screenHeight;

    private void Start()
    {
        screenWidth = Screen.width;
        screenHeight = Screen.height;
    }

    // Update is called once per frame
    void Update()
    {
        CalculatePosition();
    }

  

    void CalculatePosition()
    {

        CheckInputs();

        //CheckMousePosition();

        float finalDeltaX = xDelta * horizontalSpeed * Time.deltaTime;
        float finalDeltaY = yDelta * zoomSpeed * Time.deltaTime;
        float finalDeltaZ = zDelta * horizontalSpeed * Time.deltaTime;

        if (finalDeltaX != 0 || finalDeltaY != 0 || finalDeltaZ != 0)
            transform.Translate(new Vector3(finalDeltaX, finalDeltaY, finalDeltaZ),Space.World);

        Vector3 clampedPos = transform.position;
        clampedPos.y = Mathf.Clamp(clampedPos.y, minZoom, maxZoom);
        transform.position = clampedPos;

        cam.transform.position = Vector3.Lerp(cam.transform.position, transform.position, Time.deltaTime * 5);

        float normalizedZoom = Mathf.InverseLerp(minZoom,maxZoom, cam.transform.position.y);
        float camRotation = rotationCurve.Evaluate(normalizedZoom);
        cam.transform.rotation = Quaternion.Euler(Vector3.right * camRotation);
    }
    void CheckInputs()
    {
        xDelta = Input.GetAxis(HORIZONTAL_AXIS);
        yDelta = Input.GetAxis(MOUSE_SCROLL_WHEEL);
        zDelta = Input.GetAxis(VERTICAL_AXIS);
    }

     void  CheckMousePosition()
    {
        Vector2 mousePos = Input.mousePosition;
        if(mousePos.x <  screenMovingMargin)
        {
            xDelta = -1;
        }
        else if (mousePos.x > screenWidth - screenMovingMargin)
        {
            xDelta = 1;
        }

        if(mousePos.y < screenMovingMargin)
        {
            zDelta = -1;
        }
        else if(mousePos.y > screenHeight-screenMovingMargin)
        {
            zDelta = 1;
        }

    }



}
