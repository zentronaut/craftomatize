﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameSceneManager : MonoBehaviour
{


    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;
    }
    public void LoadSceneLayer(int _sceneIndex)
    {
        SceneManager.LoadScene(_sceneIndex,LoadSceneMode.Additive);
    }

    void OnSceneLoaded(Scene scene,LoadSceneMode mode)
    {
        SceneManager.SetActiveScene(scene);
       
    }
}
