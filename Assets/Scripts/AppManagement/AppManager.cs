﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppManager : MonoBehaviour
{

    public static AppManager Instance;

    public GameSceneManager sceneManager;
    // Start is called before the first frame update
    void Awake()
    {
        if(Instance == null)
        Instance = this;
        else {
            Destroy(gameObject); 
        }
      
    }

    private void Start()
    {
        sceneManager.LoadSceneLayer(1);
    }
}
