﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class ItemData 
{
   
    public int quantity=1;

    public ItemDefinition definition;


    public ItemData (ItemType _type)
    {
        definition = GameManager.Instance.definitionsManager.GetItemDefinition(_type);
    }

    public bool TryAddToIntemIntoStack()
    {
        if (quantity < definition.stackSize)
        {
            quantity++;
            return true;
        }
        return false;
    }

    public bool TryToRemoveItemFromStack()
    {
        if (quantity > 1)
        {
            quantity--;
            return true;
        }
        else
        {
            quantity--;
            return false;
        }
    }
}
