﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InstructionType { MoveForward, TurnRight, TurnLeft, SingleDrill,Pick,Drop,Store,PullOutItem};
public enum BehaviourType { Movement, Programmable,Driller,Collector,Storage};
public enum GameElementType { Structure, Building, Unit , Resource };
public enum MovementType { Continuous, Curve};
public enum UnitType { SmallRobot };
public enum BuildingType { StorageBuilding };
public enum ResourceType { Metal};
public enum Direction { Up = 0,Right = 90,Down = 180,Left = 270};
public enum ItemType {Metal,Off};