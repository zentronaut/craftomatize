﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class BehaviourData 
{
    public BehaviourType behaviourType;
    public int speedInThicks;
    public int drillPower;
   
    public MovementType movementType;
    public bool paused;
    //For programmable stuff
    public InstructionBundle instructionBundle;
    public int currentInstructionSet = 0;
    public int currentInstruction = 0;
    public int currentTick = 0;
    // For storage stuff
    public int capacity;
    public int currentNumberOfItems;
    [SerializeField]
    public List<ItemData> items;
    public bool isFull;
}
