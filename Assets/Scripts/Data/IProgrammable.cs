﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IProgrammable
{
    void ExecuteInstruction(InstructionType _instructionType);
}
