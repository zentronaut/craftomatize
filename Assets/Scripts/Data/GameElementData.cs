﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class GameElementData 
{
    [SerializeField]
    public List<BehaviourData> behaviours;
    public GameElementType gameElementType;
    public UnitType unitType;
    public BuildingType buildingType;
    public Direction rotationDirection;
    public int currentResourceQuantity;
    public Vector3 worldPos;
}
