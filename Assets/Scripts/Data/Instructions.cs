﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[System.Serializable]
public class Instruction
{
    public InstructionType instructionType;
    public BehaviourType behaviourTypeBelonging;
    public int instructionRepeatTimes;
    private int currentIteration;
    private bool initialized;
    public void Init()
    {
       
        this.instructionRepeatTimes = instructionRepeatTimes - 1;
        currentIteration = instructionRepeatTimes ;
        initialized = true;
    }
    public bool Iterate()
    {
        if (!initialized)
            Init();

        bool flag = false;
        if(instructionRepeatTimes!=-2)
        {
           
          
            flag = currentIteration == 0;
            currentIteration--;

            if (flag)
                currentIteration = instructionRepeatTimes;
        }
        return flag;
    }
}
[System.Serializable]
public class InstructionSet
{
    public List<Instruction> instructions;
    public int setRepeatTimes;
    private int currentIteration;
    private bool initialized;
    public void Init()
    {

        this.setRepeatTimes = setRepeatTimes - 1;
        currentIteration = setRepeatTimes;
        initialized = true;
    }
    public bool Iterate()
    {
        if (!initialized)
            Init();

        bool flag = false;
        if (setRepeatTimes != -2)
        {

           
            flag = currentIteration == 0;
            currentIteration--;

            if (flag)
                currentIteration = setRepeatTimes;

        }
        return flag;
    }
}
[System.Serializable]
public class InstructionBundle
{
    public List<InstructionSet> sets;
    public int bundleRepeatTimes;
    public int currentIteration;
    private bool initialized;
    public void Init()
    {

        this.bundleRepeatTimes = bundleRepeatTimes - 1;
        currentIteration = bundleRepeatTimes;
        initialized = true;
    }

    public bool Iterate()
    {
        if (!initialized)
            Init();

        bool flag = false;
        if (bundleRepeatTimes != -2)
        {
          
            flag = currentIteration == 0;

            currentIteration--;
           
            if (flag)
                currentIteration = bundleRepeatTimes;

        }
        return flag;
    }
}
/* void GetNextInstruction()
    {

      
        if (bundle.sets[currentInstructionSet].instructions[currentInstruction].Iterate())
        {
            currentInstruction++;
            if(currentInstruction >= bundle.sets[currentInstructionSet].instructions.Count)
            {
                currentInstruction = 0;
                if(bundle.sets[currentInstructionSet].Iterate())
                {
                    currentInstructionSet++;
                    if(currentInstructionSet >= bundle.sets.Count)
                    {
                        currentInstructionSet = 0;
                        if(bundle.Iterate())
                        {
                            paused = true;
                           
                        }
                    }
                }
            }
        }

        Debug.Log("EXECUTING INSTRUCTION " + bundle.sets[currentInstructionSet].instructions[currentInstruction].instructionType);
        ExecuteInstruction(bundle.sets[currentInstructionSet].instructions[currentInstruction].behaviourTypeBelonging, bundle.sets[currentInstructionSet].instructions[currentInstruction].instructionType);



    }
*/