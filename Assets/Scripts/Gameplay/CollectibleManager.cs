﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleManager : MonoBehaviour
{
    // TODO Load this prefabs with prefab loader
    public GameObject metalResourcePrefab;

    [SerializeField]
    private Transform resourceCollectiblesParent;
    private List<GameObject> resourceCollectiblesOnScene;

    private void Start()
    {
        resourceCollectiblesOnScene = new List<GameObject>();
    }

    public void CreateGameElementCollectible(GameElementDefinition data)
    {
       
    }
    public void CreateResourceCollectible(ResourceType _type, Vector3 worlPos)
    {
        Vector3 gridPos = UtilFunctions.Instance.SnapToGrid(worlPos);
      
        switch (_type)
        {
            case ResourceType.Metal:
                GameObject newCollectible = PlayerMapManager.Instance.poolManager.GetCollectible(ItemType.Metal);
                newCollectible.transform.position = gridPos;
                newCollectible.SetActive(true);
                resourceCollectiblesOnScene.Add(newCollectible);
                break;
        }

    }

    public void RemoveResourceCollectible(GameObject collectible)
    {
        resourceCollectiblesOnScene.Remove(collectible);
        collectible.SetActive(false);
    }

    public GameObject GetCollectibleAtPosition(Vector3 worldPos)
    {
        GameObject newCollectible=null;

        foreach(GameObject collectible in resourceCollectiblesOnScene)
        {
            if(UtilFunctions.Instance.SnapToGrid(collectible.transform.position) == worldPos)

            {
                newCollectible = collectible;
                break;
            }
        }

        return newCollectible;
    }
   

}
