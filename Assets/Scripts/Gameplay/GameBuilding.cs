﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameBuilding : GameElement
{

    public BuildingInput[] buildingInputs;
    public BuildingOutput[] buildingOutputs;

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(RegisterInOuts());
    }

    IEnumerator RegisterInOuts()
    {
        yield return new WaitForEndOfFrame();
        for(int i=0; i< buildingInputs.Length;i++)
        {
            PlayerMapManager.Instance.inOutManager.AddInput(buildingInputs[i], buildingInputs[i].transform.position);
            buildingInputs[i].InitializeInputs();
        }
        for (int i = 0; i < buildingOutputs.Length; i++)
        {
            PlayerMapManager.Instance.inOutManager.AddOutput(buildingOutputs[i], buildingOutputs[i].transform.position);
            buildingOutputs[i].InitializeOutputs();
        }
    }


}
