﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectionManager : MonoBehaviour
{

    public GameElement selectedElement;
    public GameElement highlightedElement;

    public delegate void SelectionAction(GameElement element);
    public SelectionAction onGameElementSelected;
    public SelectionAction onGameElementDeselected;

    public void SelectingElement(GameElement _element)
    {
        if(selectedElement != _element)
        {
          
            selectedElement = _element;
            Debug.Log("Selected " + selectedElement.name);
            PlayerMapManager.Instance.uiPlayerMap.SelectGameElement(selectedElement);
        }
    }
    public void HighlightElement(GameElement _element)
    {
        if (highlightedElement != _element)
        {
            highlightedElement = _element;
            PlayerMapManager.Instance.uiPlayerMap.HighlightGameElement(selectedElement);
        }
    }
    public void Deselect()
    {
        PlayerMapManager.Instance.uiPlayerMap.Deselect();
        selectedElement = null;
    }
    public void Dehighlight()
    {
        PlayerMapManager.Instance.uiPlayerMap.Dehighlight();
        highlightedElement = null;
    }
}
