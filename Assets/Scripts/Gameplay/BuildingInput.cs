﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingInput : MonoBehaviour
{
    GameBuilding building;
    StorageBehaviour storeBehaviour;

    public void InitializeInputs()
    {
        building = GetComponentInParent<GameBuilding>();
        storeBehaviour =(StorageBehaviour) building.GetBehaviour(BehaviourType.Storage);
    }

    public bool TryToStore(Item _item)
    {
        return storeBehaviour.ReceiveItem(_item);
    }

}
