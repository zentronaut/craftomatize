﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingOutput : MonoBehaviour
{
    GameBuilding building;
    StorageBehaviour storeBehaviour;

    public void InitializeOutputs()
    {
        building = GetComponentInParent<GameBuilding>();
        storeBehaviour = (StorageBehaviour)building.GetBehaviour(BehaviourType.Storage);
    }

}
