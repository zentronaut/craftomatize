﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameResource : GameElement
{
    public int currentResource;

    private void Start()
    {
        currentResource = definition.resourceAmount;
    }
    public void Extract()
    {
        currentResource--;
        if(currentResource <= 0)
        {
            Destroy(gameObject);
        }
    }

}
