﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameElement : MonoBehaviour
{

   
    public GameElementDefinition definition;
    [SerializeField]
    public GameElementData data;

    public Dictionary<BehaviourType, Behaviour> gameElementBehaviours;

   

    public void InitializeGameElement()
    {
    
        gameElementBehaviours = new Dictionary<BehaviourType, Behaviour>();
        for (int i = 0; i < data.behaviours.Count; i++)
        {
            Behaviour newBehaviour = new Behaviour();
            switch(data.behaviours[i].behaviourType)
            {
                case BehaviourType.Movement:
                    newBehaviour = gameObject.AddComponent<MovementBehaviour>().Initialize(data.behaviours[i]);
                    break;
                case BehaviourType.Programmable:
                    newBehaviour = gameObject.AddComponent<ProgrammableBehaviour>().Initialize(data.behaviours[i]);
                    break;
                case BehaviourType.Driller:
                    newBehaviour = gameObject.AddComponent<DrillerBehaviour>().Initialize(data.behaviours[i]);
                    break;
                case BehaviourType.Collector:
                    newBehaviour = gameObject.AddComponent<CollectorBehaviour>().Initialize(data.behaviours[i]);
                    break;
                case BehaviourType.Storage:
                    newBehaviour = gameObject.AddComponent<StorageBehaviour>().Initialize(data.behaviours[i]);
                    break;
            }
            gameElementBehaviours.Add(data.behaviours[i].behaviourType, newBehaviour);
        }

    }

    public Behaviour GetBehaviour(BehaviourType type)
    {
        Behaviour output;
        gameElementBehaviours.TryGetValue(type, out output);
        return output;
    }

    void OnMouseEnter()
    {
        Debug.Log("Entering  " + gameObject.name);
        PlayerMapManager.Instance.selectionManager.HighlightElement(this);

    }

    private void OnMouseOver()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Debug.Log("Selecting " + gameObject.name);
            PlayerMapManager.Instance.selectionManager.SelectingElement(this);
        }
    }

    void OnMouseExit()
    {
        PlayerMapManager.Instance.selectionManager.Dehighlight();
    }

}
