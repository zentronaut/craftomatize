﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UIDisplayConsole : MonoBehaviour
{
    [SerializeField]
    GameElement displayingElement;
    public GameObject displayConsole;
    public TextMeshProUGUI elementName;
    public TextMeshProUGUI elementStorage;
    public TextMeshProUGUI elementResources;
    public TextMeshProUGUI elementSpeed;

    public void DisplayGameElement(GameElement _element)
    {
        displayingElement = _element;

        displayConsole.SetActive(true);
        elementName.text = _element.name;
        elementStorage.enabled = false;
        elementResources.enabled = false;
        elementSpeed.enabled = false;
        foreach (BehaviourData behaviour in _element.data.behaviours)
        {
           switch(behaviour.behaviourType)
            {
                case BehaviourType.Storage:
                    elementStorage.enabled = true;
                    elementStorage.text = "Current Stored Items: " + behaviour.currentNumberOfItems.ToString();
                    break;
                case BehaviourType.Movement:
                    elementSpeed.enabled = true;
                    elementSpeed.text ="Movement speed: " + behaviour.speedInThicks;
                    break;

            }
        }

        if(_element.data.gameElementType == GameElementType.Resource)
        {
            elementResources.enabled = true;
            elementResources.text = "Resources: "+  _element.data.currentResourceQuantity.ToString();
        }
       

    }
    public void TurnOffDisplay()
    {
        displayConsole.SetActive(false);
    }

}
