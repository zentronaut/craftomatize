﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPlayerMap : MonoBehaviour
{
   
    public UIDisplayConsole console;


    public void SelectGameElement(GameElement _element)
    {
        console.DisplayGameElement(_element);
    }

    public void Deselect()
    {
        console.TurnOffDisplay();
    }
    public void HighlightGameElement(GameElement _element)
    {
      
    }

    public void Dehighlight()
    {

    }
}
