﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UtilFunctions : MonoBehaviour
{
    public static UtilFunctions Instance;
    // Start is called before the first frame update
    void Start()
    {
        Instance = this;
    }

    public Vector3 SnapToGrid(Vector3 _worldPos)
    {
        return new Vector3(Mathf.RoundToInt(_worldPos.x), Mathf.RoundToInt(_worldPos.y), Mathf.RoundToInt(_worldPos.z));
    }
}
