﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pool : MonoBehaviour
{
    public GameObject objectToPool;
    private List<GameObject> objects;
    public int poolSize;
    public bool canGrow;

    private void Start()
    {
        objects = new List<GameObject>();
        InitPool();
    }
    void InitPool()
    {
        for(int i=0; i<poolSize;i++)
        {
            GameObject newGameObject = Instantiate(objectToPool);
            newGameObject.SetActive(false);
            newGameObject.transform.SetParent(this.transform);
            objects.Add(newGameObject);
        }
    }

    public GameObject GetObject()
    {
        GameObject pulledObject=null;

        for(int i=0;i<objects.Count;i++)
        {
            if(!objects[i].activeInHierarchy)
            {
                pulledObject = objects[i];
                break;
            }
        }

        if(!pulledObject && canGrow)
        {
            pulledObject = Instantiate(objectToPool);
            pulledObject.SetActive(false);
            pulledObject.transform.SetParent(this.transform);
            objects.Add(pulledObject);
        }



        return pulledObject;
    }
}
