﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (fileName = "NewGameElementDefinition", menuName = "GameElementDefinition" )]
public class GameElementDefinition : ScriptableObject
{
    public ResourceType resourceType;
    public int resourceAmount;
    public Vector3 miniaturePosition;
    public int numberOfNeededTilesX;
    public int numberOfNeededTilesZ;
}
