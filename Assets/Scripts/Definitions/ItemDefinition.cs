﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "NewItemDefinition", menuName = "ItemDefinition")]
public class ItemDefinition : ScriptableObject
{
    public ItemType itemType;
    public bool stackeable;
    public int stackSize;

}
