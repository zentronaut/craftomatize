﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectorBehaviour : Behaviour,IProgrammable
{
    Transform miniatureParent;
    GameObject pickedElement;
    StorageBehaviour storageBehaviour;
    public CollectorBehaviour Initialize(BehaviourData _data)
    {
        this.behaviourData = _data;
        return this;
    }

    void Start()
    {
       
    }

    // Start is called before the first frame update
    public override void ExecuteInstruction(InstructionType instructionType)
    {
        if (!this.behaviourData.paused)
            switch (instructionType)
            {
                case InstructionType.Pick:
                    Pick();
                    break;
                case InstructionType.Drop:
                    Drop();
                    break;
                case InstructionType.Store:
                    Store();
                    break;
                case InstructionType.PullOutItem:
                    PullOut();
                    break;
            }
    }

    void Pick()
    {
        Debug.Log("Picking");
        GameObject collectibleToPick = PlayerMapManager.Instance.collectiblesManager.GetCollectibleAtPosition(gameElement.transform.position + gameElement.transform.forward);
        if(!collectibleToPick)
            collectibleToPick = PlayerMapManager.Instance.collectiblesManager.GetCollectibleAtPosition(gameElement.transform.position);

        if (!collectibleToPick)
        {
            Debug.Log("WARNING nothing to pick up");
        }
        else
        {
            pickedElement = collectibleToPick;
            collectibleToPick.transform.SetParent(gameElement.transform);
            collectibleToPick.transform.localPosition = gameElement.definition.miniaturePosition;
        }

    }
     
    void Drop()
    {
        if(pickedElement) //if has an element
        {
            //Check if there is input infront

            BuildingInput buildingInput = PlayerMapManager.Instance.inOutManager.GetInputAt(gameElement.transform.position + gameElement.transform.forward);

            if(buildingInput)
            {
                if(buildingInput.TryToStore(pickedElement.GetComponent<Item>()))
                {
                    pickedElement.transform.SetParent(null);
                    PlayerMapManager.Instance.collectiblesManager.RemoveResourceCollectible(pickedElement);
                    pickedElement = null;
                }

            }
            else // Check if there is an obstacle infrot or if not it drops the item
            { //TODO add all building collisions in worldBuilder
                if(!PlayerMapManager.Instance.worldBuilder.IsObstacleAtWorldPosition(gameElement.transform.position + gameElement.transform.forward))
                {

                    pickedElement.transform.SetParent(null);
                    pickedElement.transform.position = UtilFunctions.Instance.SnapToGrid(gameElement.transform.position + gameElement.transform.forward);
                    pickedElement = null;
                }
                else
                {

                    Debug.Log("ENTRANCE BLOCKED");
                }

            }


        }
        else
        {
            Debug.Log("WARNING nothing to Drop");
        }
    }

    void Store()
    {
        if (pickedElement)
        {

            if (!storageBehaviour)
                storageBehaviour = (StorageBehaviour)gameElement.GetBehaviour(BehaviourType.Storage);
            if (storageBehaviour)
            {
                if (storageBehaviour.ReceiveItem(pickedElement.GetComponent<Item>()))
                {
                    pickedElement.transform.SetParent(null);
                    PlayerMapManager.Instance.collectiblesManager.RemoveResourceCollectible(pickedElement);
                    pickedElement = null;
                }

            }
        }
    }
    /// <summary>
    /// Pulls out the first item on the stored behaviour
    /// </summary>
    void PullOut()
    {
        if(!pickedElement)
        {
            if (!storageBehaviour)
                storageBehaviour = (StorageBehaviour)gameElement.GetBehaviour(BehaviourType.Storage);
            if (storageBehaviour)
            {
                ItemType pickedItem = storageBehaviour.PickFirstItem();
                if (pickedItem != ItemType.Off)
                {
                    pickedElement = PlayerMapManager.Instance.poolManager.GetCollectible(pickedItem);
                    pickedElement.transform.SetParent(gameElement.transform);
                    pickedElement.transform.localPosition = gameElement.definition.miniaturePosition;
                    pickedElement.gameObject.SetActive(true);
                }
                else
                {
                    Debug.Log("No more stored items");
                }
            }
        }
        else
        {
            Debug.Log("WARNING CANT HOLD 2 ITEMS AT ONCE");
        }
    }
}
