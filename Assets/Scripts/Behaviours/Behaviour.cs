﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Behaviour : MonoBehaviour
{

    public BehaviourData behaviourData;

    protected GameElement gameElement;

    // Start is called before the first frame update
    void Awake()
    {
        gameElement = GetComponentInParent<GameElement>();
    }

    public virtual void ExecuteInstruction(InstructionType instructionType)
    {

    }
}
