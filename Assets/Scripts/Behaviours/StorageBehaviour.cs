﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StorageBehaviour : Behaviour, IProgrammable
{
 
    BehaviourData behaviourData;
    public Vector3[] receivePositions;
    public Vector3 dropPosition;

    public StorageBehaviour Initialize( BehaviourData _behaviourData)
    {
        behaviourData = _behaviourData;
        return this;
    }

    public override void ExecuteInstruction(InstructionType instructionType)
    {
        switch(instructionType)
        {
            case InstructionType.Drop:

                break;
        }
    }


    public bool ReceiveItem(Item _newItem)
    {
        bool itemReceived = false;
        if (!behaviourData.isFull)
        {
            foreach (ItemData item in behaviourData.items)  // Try to add it into stack if it's possible
            {
                if (item.definition.itemType == _newItem.itemType && item.definition.stackeable)
                {
                    if (item.TryAddToIntemIntoStack())
                    {
                        behaviourData.currentNumberOfItems++;
                        return true;
                    }

                }
            }

            // If can't add to stack, try to add in new space
            if (behaviourData.currentNumberOfItems < behaviourData.capacity)
            {
              
                itemReceived = true;
                ItemData newItem = new ItemData(_newItem.itemType);
                behaviourData.items.Add(newItem);
                behaviourData.currentNumberOfItems++;
            }
            else
            {

                behaviourData.isFull = true;
            }
        }
        return itemReceived;
    }

    public ItemType PickFirstItem()
    {

        ItemData selectedItem = null;
        if(behaviourData.items.Count > 0)
        {
            selectedItem = behaviourData.items[0];

            if (behaviourData.items[0].definition.stackeable)
            {
                if(behaviourData.items[0].TryToRemoveItemFromStack())
                {

                }
                else
                {
                    behaviourData.items.RemoveAt(0);
                    behaviourData.currentNumberOfItems--;
                    behaviourData.isFull = false;
                }
            }
            else
            {
                behaviourData.items.RemoveAt(0);
                behaviourData.currentNumberOfItems--;
                behaviourData.isFull = false;
            }
        }
        else
        {
            return ItemType.Off;
        }


        return selectedItem.definition.itemType;
       
    }

    public bool TryToDrop(Item itemToDrop)
    {
        bool ableToDrop=false;




        return ableToDrop;
    }




}
