﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementBehaviour : Behaviour, IProgrammable
{
    private MovementType movementType;

    public MovementBehaviour Initialize(BehaviourData _behaviourData)
    {
        behaviourData = _behaviourData;
        return this;
    }

    // Start is called before the first frame update
    public override void ExecuteInstruction(InstructionType instructionType)
    {
        if(!this.behaviourData.paused)
        switch(instructionType)
        {
            case InstructionType.MoveForward:
                StepForward();
                break;
            case InstructionType.TurnLeft:
                TurnLeft();
                break;
            case InstructionType.TurnRight:
                TurnRight();
                break;
        }
    }

    private void StepForward()
    {
        transform.position = transform.position + transform.forward;
    }
    private void TurnLeft()
    {
        transform.Rotate(new Vector3(0,1,0),-90);
    }
    private void TurnRight()
    {
        transform.Rotate(new Vector3(0, 1, 0),90);
    }

    bool IsObstacleInfront()
    {
        Debug.Log("Sent coord"  + (transform.position + transform.forward).ToString());
       return  (PlayerMapManager.Instance.worldBuilder.GetValueAtWorldPosition(transform.position + transform.forward) != 0);
    }
}
