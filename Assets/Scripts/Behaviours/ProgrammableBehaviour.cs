﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProgrammableBehaviour : Behaviour
{


    public ProgrammableBehaviour Initialize(BehaviourData _behaviourData)
    {
        behaviourData = _behaviourData;
        behaviourData.currentTick = behaviourData.speedInThicks;
        TimeManager.onGameTick += CountTick;
        return this;
    }


    void CountTick()
    {
        if (!this.behaviourData.paused && behaviourData.instructionBundle != null)
        {
            behaviourData.currentTick--;
            if (behaviourData.currentTick == 0)
            {

                GetNextInstruction();
                behaviourData.currentTick = behaviourData.speedInThicks;
            }
        }
    }

    void SetInstructionSpeed()
    {
       
    }

    void GetNextInstruction()
    {

        if(behaviourData.instructionBundle.currentIteration == -1)
        {
            this.behaviourData.paused = true;
            return;
        }

       // Debug.Log("EXECUTING INSTRUCTION " + bundle.sets[currentInstructionSet].instructions[currentInstruction].instructionType + " REPEAT " + bundle.sets[currentInstructionSet].instructions[currentInstruction].instructionRepeatTimes);
        ExecuteInstruction(behaviourData.instructionBundle.sets[behaviourData.currentInstructionSet].instructions[behaviourData.currentInstruction].behaviourTypeBelonging, behaviourData.instructionBundle.sets[behaviourData.currentInstructionSet].instructions[behaviourData.currentInstruction].instructionType);


        if (behaviourData.instructionBundle.sets[behaviourData.currentInstructionSet].instructions[behaviourData.currentInstruction].Iterate())
        {
            behaviourData.currentInstruction++;
            if (behaviourData.currentInstruction >= behaviourData.instructionBundle.sets[behaviourData.currentInstructionSet].instructions.Count)
            {
                behaviourData.currentInstruction = 0;
                if (behaviourData.instructionBundle.sets[behaviourData.currentInstructionSet].Iterate())
                {
                    behaviourData.currentInstructionSet++;
                    if (behaviourData.currentInstructionSet >= behaviourData.instructionBundle.sets.Count)
                    {
                        behaviourData.currentInstructionSet = 0;
                        if(behaviourData.instructionBundle.Iterate())
                        {
                            this.behaviourData.paused = true;
                            return;
                        }
                    }
                }
            }
        }


   

    }

    void ExecuteInstruction(BehaviourType behaviourType, InstructionType instructionType)
    {
        Behaviour behaviour;

     

        behaviour = gameElement.GetBehaviour(behaviourType);
        if (behaviour != null)
        {
            if (this.behaviourData.speedInThicks != behaviour.behaviourData.speedInThicks)
            {
                this.behaviourData.speedInThicks = behaviour.behaviourData.speedInThicks;
                this.behaviourData.currentTick = behaviour.behaviourData.speedInThicks;
            }
            behaviour.ExecuteInstruction(instructionType);
        }
        else
        {
            Debug.Log("No Behaviour attaached of type " + behaviourType.ToString() + " for element " + gameElement.name);
        }

    }

   

}

