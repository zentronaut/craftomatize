﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrillerBehaviour : Behaviour, IProgrammable
{

    public int drillPower;

    private GameResource currentResource;

    public DrillerBehaviour Initialize(BehaviourData _data)
    {
        this.behaviourData = _data;
        return this;
    }

    public override void ExecuteInstruction(InstructionType instructionType)
    {
       
        if (!this.behaviourData.paused)
            switch (instructionType)
            {
                case InstructionType.SingleDrill:
                    CheckIfCanDrill();
                    break;
               
            }
    }

    //TODO RESET current resource when it movessss
    void CheckIfCanDrill()
    {
      // Debug.Log("Checking if can drill ");
        //TODO CHANGE THIS TO CHECK A RESOURCE MANAGER POSITIONS
        GameElement elementInFront = PlayerMapManager.Instance.worldManager.GetResourcesAtWorldPosition(gameElement.transform.position + gameElement.transform.forward);

        if (!elementInFront)
        {
            Debug.Log("Nothing to drill");
            return;
        }

        if(elementInFront.data.gameElementType == GameElementType.Resource && elementInFront.definition.resourceType == ResourceType.Metal)
        {
            currentResource = elementInFront.GetComponent<GameResource>();

            if(currentResource.definition.resourceAmount > 0)
            {
                currentResource.Extract();
                CreateCollectibleResource(elementInFront.definition.resourceType);
            }
        }
        else
        {
            Debug.Log("Nothing to drill");
        }
    }
    void CreateCollectibleResource(ResourceType _type)
    {
       // Debug.Log("Crreating collectible");
        PlayerMapManager.Instance.collectiblesManager.CreateResourceCollectible(_type,gameElement.transform.position - gameElement.transform.forward);
    }

   
}

